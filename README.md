# Slalom Demo - Build System API for Customer backend

## What are System APIs?

System APIs provide a means for insulating the data consumers from the complexity or changes to the underlying systems. Once built, many consumers can access data without any need to know the details of the underlying systems. System APIs are typically fine-grained, business process-independent, and highly reusable.

## Use case

Slalom’s customer data is stored in MySql (hosted on Azure). Consumers like mobile app, web app need to access the customer info. And there will be many more consumers interested in signing up to consume the customer data in near future.

## Operations

1.	Get all customers
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers

2.	Get a particular customer
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers/2

3.	Create single customer
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers

4.	Create customers – batch mode
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers/batch

5.	Update a particular customer
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers/4

6.	Delete a customer
http://slalomdemo.us-e2.cloudhub.io/api/slalom/customers/3

Refer to the Postman collection (slalom-demo.postman_collection.json included in docs section under src/main/resources/docs) for additional information about the operations with examples.
